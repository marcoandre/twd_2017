Django==1.9
django-bootstrap-toolkit==2.15.0
django-registration-redux==1.4
olefile==0.44
Pillow==4.2.1
